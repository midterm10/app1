package com.example.app1

import android.content.Intent
import android.os.Bundle
import android.provider.AlarmClock.EXTRA_MESSAGE
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val btn = findViewById<Button>(R.id.btnhello)
        val name = findViewById<TextView>(R.id.textname)
        val sentname = name.text.toString()

        btn.setOnClickListener{
            val i = Intent(this, HelloActivity::class.java).apply {
                putExtra(EXTRA_MESSAGE, sentname)
            }
            startActivity(i)
        }
    }
}